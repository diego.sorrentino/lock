<?php
namespace DiegoSorrentino\Library;

class Lock{
	private string $path = './lock/';
	private string $filename = 'file.lock';
	private int $maxLifeInMin = 30;

	private string $filepath;

	private bool $directoryExists = false;
	private bool $lockCreated = false;
	private bool $fileExists = false;
	private bool $toKill = false;
	private bool $killedFrozenProcess = false;
	private int $myPid;
	
	public function __construct(
			?string $path = null, 
			?string $filename = null,
			?int $maxLifeInMin = null){
		$dirCreated = false;
		$lockCreated = false;

		$this->myPid = getmypid();

		if(!empty($path)) $this->path = realpath('./' . $path);
		if(!empty($filename)) $this->filename = $filename;
		if(!empty($maxLifeInMin)) $this->maxLifeInMin = $maxLifeInMin;

		$this->directoryExists = $this->mkPath();
		$this->filepath = sprintf('%s/%s', $this->path, $this->filename);
		
		if( ! file_exists($this->filepath)){ 
			$this->lockCreated = $this->mkLock();
			return;
		}
		//else lock already exists... but i must check if is started before than $maxLifeInMin
		$this->checkExpireTime();
		if($this->toKill && $this->killedFrozenProcess)
			$this->lockCreated = $this->mkLock();
		
	}

	public function lockAcquired():bool { return $this->directoryExists & $this->lockCreated; }
	public function haveYouKilledFrozenProcess():bool { return $this->toKill & $this->killedFrozenProcess; }
	public  function release():bool{
		$released = unlink($this->filepath);
		
		error_log($released 
				? "[INF] Lock released"
				: "[ERR] Impossible release lock"
		);
		return $released;
	}

	private function checkExpireTime(){
		$now = time();

		$ctime = filectime($this->filepath);
		$this->toKill = ($ctime + $this->maxLifeInMin*60 < $now) ? true : false;
		if( ! $this->toKill){
			error_log("[INF] {$this->filepath} <= maxLifeInMin: i wait");
			return;
		}
		
		error_log("[ERR] {$this->filepath} > maxLifeInMin: process must be killed");
		$this->killFrozenProcess();
	}

	private function killFrozenProcess(){
		error_log("[INF] trying to delete {$this->filepath}");

		$pid = file_get_contents($this->filepath);
		unlink($this->filepath);
		posix_kill($pid, SIGKILL);
		$this->killedFrozenProcess = true;
	}


	private function mkPath():bool{
		if( ! file_exists($this->path) ){
			mkdir($this->path, 0755, true);
			//on error i'll exit in next check ( ! is_dir() )
		}

		if( ! is_dir($this->path)){
			error_log("[ERR] {$this->path} exists, NOT is_dir");
			return false;
		}

		if( ! is_writable($this->path)){
			error_log("[ERR] {$this->path} exists, is_dir but NOT is_writable");
			return false;
		}

		error_log("[INF] {$this->path} exists, is_dir and is_writable");
		return true;
	}

	private function mkLock():bool{
		$handler = fopen($this->filepath, 'w');

		if( false == $handler ){
			error_log("[ERR] I cant create lock file");
			return false;
		}

		fwrite($handler, $this->myPid);
		fclose($handler);
		return true;
	}



}

?>
